﻿import java.util.Scanner;
import java.math.BigDecimal;

class Fahrkartenautomat
{
//Methode fahrkartenbestellungErfassen
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		double zuZahlenderBetrag = 0.0 ;
		int anzahlDerTickets = 0;
		double zuZahlenderGesamtBetrag = 0.0;
		int wahl = 0 ; // Variable für verschiedenen Arten von Fahrkarten
// für Fahrkartenautomat 06 
	do {	
		System.out.println("wählen sie:\n"
				+ "Einzelfahrscheine (1)\n"
				+ "Tageskarte (2)\n"
				+ "Gruppenkarten (3)\n"
				+ "bezahlen (9)\n"); 
 
		/*System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
				+ "Einzelfahrschein Regeltarif AB [2,90€] = 1 \n"
				+ "Tageskarte Regeltarif AB [8,60€] = 2 \n"
				+ "Kleingruppen-Tageskarte Regeltarif AB [23,50€] = 3 \n"); */
			
	
		System.out.print("Ihre Wahl:");
		wahl = tastatur.nextInt();
		
	  while(wahl< 1 || (wahl> 3 && wahl != 9)); {
			System.out.println(">>Falsche Eingabe<<");
			System.out.print("Ihre Wahl: ");
			wahl = tastatur.nextInt();
		} 
		
		switch(wahl) {
		  
		case 1:
			zuZahlenderBetrag = 2.90;
			
			System.out.println("Einzelfahrschein Regeltarif AB");
			//System.out.printf("Zu zahlender Betrag ist: %.2f€\n ", zuZahlenderBetrag);
			System.out.printf("Fahrkartenpreis ist: %.2f€\n", zuZahlenderBetrag);
			break;
		
		case 2:
			zuZahlenderBetrag = 8.60;
			  
			  System.out.println("Tageskarte Regeltarif AB");
			//System.out.printf("Zu zahlender Betrag ist: %.2f€\n ", zuZahlenderBetrag );
			  System.out.printf("Fahrkartenpreis ist: %.2f€\n", zuZahlenderBetrag);
			break;
		
		case 3:
			zuZahlenderBetrag = 23.50;
			
			System.out.println("Kleingruppen-Tageskarte Regeltarif AB");
			//System.out.printf("Zu zahlender Betrag ist: %.2f€:\n ", zuZahlenderBetrag);
			System.out.printf("Fahrkartenpreis ist: %.2f€\n", zuZahlenderBetrag);
			break;
		case 9:
			if(zuZahlenderGesamtBetrag >0)
			return zuZahlenderGesamtBetrag;
			else {
				System.out.println("Sie können doch nicht bezahlen!"
						+ "ohne zu wählen, was für Fahrkartenarten "
						+ "Sie wollen.\n brechen und wiederholen Sie bitte "
						+ "den Vorgang. Wählen Sie ein oder mehrere"
						+ "Fahrkartenarten aus. Danke!");
				return 0;
			}
			
	}
	
		//System.out.print("Zu zahlender Betrag (Euro):\n ");
		//zuZahlenderBetrag = tastatur.nextDouble();
		
// fürFahrkartenautomat 05++
  do {  System.out.print("Anzahl Der Tickets:");
	     anzahlDerTickets = tastatur.nextInt();
			
		}while(anzahlDerTickets < 1 || anzahlDerTickets > 10);
  
      zuZahlenderGesamtBetrag  += zuZahlenderBetrag * anzahlDerTickets;
       
       
}while(wahl!= 9);
// fürFahrkartenautomat 05
	  /*	if(anzahlDerTickets <= 0 || anzahlDerTickets > 10) { 
			
		
			System.out.println("Achtung! diese Nachricht erscheint,\n wenn "
				+ "ein tippfehler  entstanden ist.\n die angebene anzahlTicket "
				+ "war nicht korrekt");
		    anzahlDerTickets = 1;
			
		} */
		
		//zuZahlenderGesamtBetrag  = zuZahlenderBetrag * anzahlDerTickets;
		//zuZahlenderGesamtBetrag += zuZahlenderGesamtBetrag;
	
		return zuZahlenderGesamtBetrag;

	} 
	
//Methode fahrkartenBezahlen
	public static double fahrkartenBezahlen(double zuZahlenderGesamtBetrag) {
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		
       while(eingezahlterGesamtbetrag < zuZahlenderGesamtBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro %n" , (zuZahlenderGesamtBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
		
	    return eingezahlterGesamtbetrag;
			
	}
	
	//Methode fahrkartenAusgeben
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          
	          warte(250);
	          
	       }
	       System.out.println("\n\n");
	}
	
	//Methode warte
	public static void warte(int ms) {
		
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//Methode rueckgeldAusgeben
	public static double rueckgeldAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderGesamtBetrag) {
		double rückgabebetrag;
		
		rückgabebetrag = eingezahlterGesamtbetrag*100.0 - zuZahlenderGesamtBetrag*100.0; 
		rückgabebetrag /= 100;
	       
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	   muenzeAusgeben(2,"Euro");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	   muenzeAusgeben(1,"Euro");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	   muenzeAusgeben(50,"Cent");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	   muenzeAusgeben(20,"Cent");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	   muenzeAusgeben(10,"Cent");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(5,"Cent");
	 	          rückgabebetrag -= 0.05;
	           }
	          
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
		return rückgabebetrag;
	}
	
	//Methode muenzeAusgeben
	public static void muenzeAusgeben(int betrag,String einheit) {
		
		System.out.println(betrag +" "+ einheit);		
	}
	
	
    public static void main(String[] args)
    {
      /* Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0.0; // Einzelpreis 
       double eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int anzahlDerTickets;// Neue deklariert Variable,für die Eingabe Anzahl des Tickets
       double zuZahlenderGesamtBetrag = 0.0;// Neue deklariert Variable, für den zu Zahlenden gesamt Betrag
       */
       
       
       double f1 = fahrkartenbestellungErfassen();// Aufruf der Methode fahrkartenbestellungErfassen
       System.out.printf("Der zu zahlender gesamt Betrag ist: %s Euro %n", f1);
       
     /*System.out.print("Zu zahlender Betrag (EURO):\n ");
        zuZahlenderBetrag = tastatur.nextDouble();
       System.out.println("zu zahlen Betrag ist:"+ zuZahlenderBetrag);
       
       System.out.print("Anzahl Der Tickets: ");
        anzahlDerTickets = tastatur.nextInt();// als integer, denn Anzahl der Tickets muss als ganzzahl deklariert werden
        zuZahlenderGesamtBetrag = zuZahlenderBetrag * anzahlDerTickets;*/// 
       /*bei der Berechnung
       dieses Ausdruks wird jedesmal die Einzelpreis des Ticktes ZB. 2,5 euro mit der
       Anzahl des Tickets ZB. 2 oder mehr multipliziert und das daraus resultierende 
       Ergebnis , wird in der zuZahlenderGesamtBetrag Variable gespeichert. */
       
       

       // Geldeinwurf  
       // -----------
    //  eingezahlterGesamtbetrag = 0.0;
      // while(eingezahlterGesamtbetrag < zuZahlenderGesamtBetrag)
             //Geld  wird solange angefragt,  bis der mindestens Zu zahlende gesamt Betrag erreicht ist.*/
       
     /*  {
    	   System.out.printf("Noch zu zahlen: %.2f Euro %n" , (zuZahlenderGesamtBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }*/
       
       double f2 = fahrkartenBezahlen(f1); //Aufruf der Methode fahrkartenBezahlen
      // System.out.printf("Der eingezahlter gesamt Betrag ist: %s Euro %n", f2);

       // Fahrscheinausgabe
       // -----------------
       
      /* System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");*/
        
     fahrkartenAusgeben(); // Aufruf der Methode fahrkartenAusgeben
       

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      // rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderGesamtBetrag; 
       // Rundungsfehler bei Gleitkommazahlen
     //  if(rückgabebetrag > 0.0)
      /* {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }*/

     /*  System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt."); */
       
       double f4 = rueckgeldAusgeben(f2,f1); //Aufruf der Methode rueckgeldAusgeben
      
    }
   
}
